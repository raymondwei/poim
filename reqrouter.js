"use strict";
const express = require('express');
var router = express.Router();
var ReqHandler = require('./modules/reqhandler').ReqHandler;

router.get('/', function(req, res){
    res.send({result:'__OK__'}); 
});

module.exports = router;
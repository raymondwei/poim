/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ***********************************************/
"use strict";
// run express serving PORT 5003
const express = require("express"); // request the package
const path = require("path");
const app = express(); // instanciate express into app
const PORT = process.env.PORT || 5003; // specify port

// define route for all /req/*
const reqrouter = require("./reqrouter");
app.use("/req", reqrouter);

// map /static to /public
app.use("/pics", express.static(path.join(__dirname, "public/pics")));
app.use("/css", express.static(path.join(__dirname, "public/css")));
app.use("/js", express.static(path.join(__dirname, "public/js")));

app.set("view engine", "pug");

/* =================================================================== */
const browser_title = "SNG poim 0.1.33";
const build_release = "build 0.1.33.20200813";
/* =================================================================== */

app.get(["/", "/index.html"], function (req, res) {
  var resp = { result: "__OK__" };
  res.render("index", {
    pugdata: JSON.stringify(resp),
    title: browser_title,
    release: build_release,
  });
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
// END run express serving PORT 5003

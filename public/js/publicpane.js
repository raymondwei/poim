/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var INFO_TEXT = `<pre>所谓工业4.0（Industry4.0）， 是基于工业发展的不同阶段作出的划分。

按照目前的共识， 工业1.0是蒸汽机时代， 工业2.0是电气化时代，工业3.0
是信息化时代，工业4.0则是利用信息化技术促进产业变革的时代，也就是
智能化时代。

这个概念最早出现在德国，  2013年的汉诺威工业博览会上正式推出， 其
核心目的是为了提高德国工业的竞争力，  在新一轮工业革命中占领先机。  
随后由德国政府列入《德国2020高技术战略》中所提出的十大未来项目之一。
该项目由德国联邦教育局及研究部和联邦经济技术部联合资助，投资预计达2亿
欧元。旨在提升制造业的智能化水平，建立具有适应性、资源效率及基因
工程学的智慧工厂，在商业流程及价值流程中整合客户及商业伙伴。

其技术基础是网络实体系统及物联网。</pre>`;

var AC_PublicPane = (function(){
    function AC_PublicPane (rp){
        this.RP = rp;
        this.AJ_div = $('<div id="left-publicpage"></div>');
    }// end of AC_PublicPane constructor

    AC_PublicPane.prototype.AF_show = function(onoff){
        if(onoff){
            this.AJ_div.empty();
            this.AJ_info = $('<div id="publicpage-info"></div>')
                .appendTo(this.AJ_div)
                .html(INFO_TEXT);
            this.AJ_div.appendTo(this.RP.AJ_left);
        } else {
            this.AJ_div.detach();
        }
        return this;
    };// end of AC_PublicPane.prototype.AF_show

    AC_PublicPane.prototype.AF_render = function(){
    };// end of AC_PublicPane.prototype.AF_render
    return AC_PublicPane;
})();

/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_Right2 = (function(){
    function AC_Right2(rp, right_pane){
        this.RP = rp;
        this.right_pane = right_pane;
        this.AF_render();
        this.AV_shown = false;
    }// end of AC_Right1 constructor

    AC_Right2.prototype.AF_render = function(){
        this.AJ_div = $('<div id="right2"></div>').appendTo(this.RP.AJ_right);
        this.AJ_picframe = $('<div id="right2-picframe"></div>')
            .appendTo(this.AJ_div);
        this.AJ_pic = $('<img/>')
            .attr('src','/pics/zanger.png')
            .appendTo(this.AJ_picframe);
        this.AJ_devinfo = $('<div id="device-info"></div>').appendTo(this.AJ_div);
        this.AJ_devinfo.text("施耐固 C-型焊钳");
        $('<div id="methods-tbar">程式设置</div>').appendTo(this.AJ_div);
        this.AJ_methods = $('<div id="methods-pane"></div>').appendTo(this.AJ_div);
        this.AF_setup_methods(this.AJ_methods);
        $('<div id="parms-tbar">焊接参数</div>').appendTo(this.AJ_div);
        var ttls = $('<div id="parm-titles"></div>').appendTo(this.AJ_div);
        $('<div class="parm-title-box">名称</div>').appendTo(ttls);
        $('<div class="parm-title-box">设置数值</div>').appendTo(ttls);
        $('<div class="parm-title-box">实测数值</div>').appendTo(ttls);
        // $('<div class="parm-title-box">结果评估</div>').appendTo(ttls);
        this.AJ_parm_pane = $('<div id="parm-pane"></div>').appendTo(this.AJ_div);
        var parmpane_h = this.RP.body_h - 670;
        this.AJ_parm_pane.css('height', parmpane_h + 'px;')
        this.AF_setup_parmrows(this.AJ_parm_pane);
        this.AJ_conclude = $('<div id="parm-conclude"></div>').appendTo(this.AJ_div);
        this.AF_setup_conclude(this.AJ_conclude);
    };// end of AC_Right2.prototype.AF_render
    
    AC_Right2.prototype.AF_setup_methods = function(jdiv){
        var row, keys = Object.keys(_WeldMethods);
        for(var i=0; i<keys.length; i++){
            row = $('<div class="method-row"></div>').appendTo(jdiv);
            $('<div class="method-name"></div>')
                .text(keys[i]).appendTo(row);
            $('<div class="method-choice"></div>')
                .text(_WeldMethods[keys[i]][0]).appendTo(row);
        }
    };// end of AC_Right2.prototype.AF_setup_methods

    AC_Right2.prototype.AF_setup_conclude = function(jdiv){
        $('<div class="parm-value-box"></div>').text("质检方式")
            .css('color','white').appendTo(jdiv);
        $('<div class="parm-value-box"></div>').text("电流检查方式")
            .css('color','white').appendTo(jdiv);
        $('<div class="parm-value-box"></div>').text("通过")
            .css('color','white').appendTo(jdiv);
    };// end of AC_Right2.prototype.AF_setup_conclude

    AC_Right2.prototype.AF_setup_parmrows = function(jdiv){
        var row, box, icon;
        for(var i=0; i<_WeldParams.length; i++){
            row = $('<div class="parm-row"></div>').appendTo(jdiv);
            box = $('<div class="parm-value-box"></div>')
                .text(_WeldParams[i][0]).appendTo(row);
            box = $('<div class="parm-value-box"></div>')
                .text(_WeldParams[i][1]).appendTo(row);
            box = $('<div class="parm-value-box"></div>')
                .text(_WeldParams[i][2]).appendTo(row);
            icon = $('<div class="info-icon"></div>');
        }
    };// end of AC_Right2.prototype.AF_setup_parmrows
        
    AC_Right2.prototype.AF_show = function(v){
        var wid = this.RP.body_w - 400;
        var self = this;
        if(v){
            this.AJ_div.animate({width: wid + 'px'}, function(e){
                self.AV_shown = true;
                return U.AF_stop_event(e);
            });
        } else {
            this.AJ_div.animate({width: '0px'}, function(e){
                self.AV_shown = false;
                return U.AF_stop_event(e);
            });
        }
    };// end of AC_Right2.prototype.AF_show
    
    return AC_Right2;
})();
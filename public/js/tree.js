/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 * <svg xmlns="http://www.w3.org/2000/svg" id="the-svg" version="1.1"><line id="TC6H8EU-PA-VUbnxU1407879034120_c_PTH_stem" x1="0" y1="55" x2="20" y2="55" class="white-line" style="visibility: hidden;"></line>
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_Tree = (function(){
    // type can be: "prod" | " equip" | "org"
    function AC_Tree(tree_pane, type){
        // tree_pane.AJ_div(div#left-tree) inside rp.AJ_left(div#container-left)
        this.tree_pane = tree_pane; 
        this.AJ_div = tree_pane.AJ_div;
        this.AV_type = type;
        this.AV_root = new AC_TreeNode(this);
        
        if(type === "prod"){
            var prod = this.AV_root.AF_populate(_DB.root)
                           .AF_show(true);
            prod.AF_addchild(_DB.base1);
            prod.AF_addchild(_DB.base2);
            prod.AF_addchild(_DB.base3);
            prod.AF_addchild(_DB.base4);

            var suv, car;

            // base-1
            var base1 = this.AV_root.AV_children[0];
            var suv = base1.AF_addchild(_CarTypes.suv);
            suv.AF_addchild(_CarModels.audi_Q5);
            suv.AF_addchild(_CarModels.audi_Q7);

            base1.AF_addchild(_CarTypes.minivan);

            // base-2
            var base2 = this.AV_root.AV_children[1];
            var car = base2.AF_addchild(_CarTypes.sedan);
            car.AF_addchild(_CarModels.benz_c300);
            car.AF_addchild(_CarModels.audi_A4);
            car.AF_addchild(_CarModels.audi_A6);
            car.AF_addchild(_CarModels.vw_jetta);
            car.AF_addchild(_CarModels.vw_golf);
            car.AF_addchild(_CarModels.bmw_500);

            // base-3
            this.AV_root.AV_children[2]
                .AF_addchild(_CarTypes.truck)
                .AF_addchild(_CarTypes.suv);
        } else if(type === "equip"){
            this.AV_root.AF_populate(_DB.root)
                .AF_addchild(_DB.base1)
                .AF_addchild(_DB.base2)
                .AF_addchild(_DB.base3)
                .AF_addchild(_DB.base4);

        } else if(type === "org"){
            this.AV_root.AF_populate(_DB.root)
                .AF_addchild(_DB.base1)
                .AF_addchild(_DB.base2)
                .AF_addchild(_DB.base3)
                .AF_addchild(_DB.base4);

        }
    }// end of AC_Tree constructor

    AC_Tree.prototype.AF_show = function(){
        this.AV_root.AF_show(true);        
    }// end of AC_Tree.prototype.AF_add_node

    return AC_Tree;
})();
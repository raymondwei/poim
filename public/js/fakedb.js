var _DB = {
    root: {
        name: "一汽奥迪集团",
        picurl: "/pics/dragon-vw.png"
    },
    base1: {
        name: "基地-1",
        picurl: "/pics/factory1.jpg"
    },
    base2: {
        name: "基地-2",
        picurl: "/pics/factory2.jpg"
    },
    base3: {
        name: "基地-3",
        picurl: "/pics/factory3.jpg"
    },
    base4: {
        name: "基地-4",
        picurl: "/pics/factory4.jpg"
    }
};
var _CarTypes = {
    suv: {
        name: "SUV",
        picurl: "/pics/cartype-suv.png"
    },
    sedan: {
        name: "轿车",
        picurl: "/pics/cartype-sedan.png"
    },
    sport: {
        name: "跑车",
        picurl: "/pics/cartype-sport.png"
    },
    minivan: {
        name: "面包车",
        picurl: "/pics/cartype-minivan.png"
    },
    truck: {
        name: "卡车",
        picurl: "/pics/cartype-truck.png"
    },
};
var _CarLogos = {
    benz: {
        name: "奔驰",
        picurl: "/pics/benz-logo.png"
    },
    audi: {
        name: "奥迪",
        picurl: "/pics/audi-logo.png"
    },
    bmw: {
        name: "宝马",
        picurl: "/pics/bmw-logo.png"
    },
    vw: {
        name: "大众",
        picurl: "/pics/vw-logo.png"
    },
    cetro: {
        name: "雪铁龙",
        picurl: "/pics/vw-logo.png"
    },
    cadi: {
        name: "卡迪拉克",
        picurl: "/pics/cai-logo.png"
    },
    car1: {
        name: "中国一汽",
        picurl: "/pics/car1-logo.png"
    },
};
var _CarModels = {
    benz_s500 : {
        name: "CC S500",
        picurl: "/pics/vw-logo.png"
    },
    benz_c300 : {
        name: "CC 300",
        picurl: "/pics/vw-logo.png"
    },
    audi_A4 : {
        name: "奥迪 A4",
        picurl: "/pics/audi-logo.png"
    },
    audi_A6 : {
        name: "奥迪 A6",
        picurl: "/pics/audi-logo.png"
    },
    audi_A8 : {
        name: "奥迪 A8",
        picurl: "/pics/audi-logo.png"
    },
    audi_Q5 : {
        name: "奥迪 Q5",
        picurl: "/pics/audi-logo.png"
    },
    audi_Q7 : {
        name: "奥迪 Q7",
        picurl: "/pics/audi-logo.png"
    },
    vw_jetta : {
        name: "大众 捷达",
        picurl: "/pics/vw-logo.png"
    },
    vw_golf : {
        name: "大众 高尔夫",
        picurl: "/pics/vw-logo.png"
    },
    bmw_500 : {
        name: "帕沙特 500",
        picurl: "/pics/vw-logo.png"
    }
};
var _Procs = { 
    metalwork : {
        name: "钣金成型",
        picurl: "/pics/metalwork.jpg"
    },
    spotweld: {
        name: "焊接",
        picurl: "/pics/zanger.png"
    },
    paint: {
        name: "涂漆",
        picurl: "/pics/paint.jpg"
    },
    assembly: {
        name: "安装",
        picurl: "/pics/assembly.jpg"
    }
};
var _VINs = [
    "LAFM5678A9331291",
    "LAFM5678A9331292",
    "LAFM5678A9331293",
    "LAFM5678A9331294",
    "L0DM5678A9331221",
    "L0FM5678A9331276",
    "L0FM5678A9331298",
    "L0FM5678A9331291",
    "L0FM5678A9331299",
    "L0FM5678A9331295",
    "LAFM5678A9331235",
    "LAFM5678A9331296",
    "LAFM5678A9331297",
    "LAFM5678A9331231",
    "LAFM5678A9331241",
    "LAFM5678A9331251",
    "LAFM5678A9331233",
    "LAFM5678A9331234",
    "LAFM5678A9331235",
    "LAFM5678A9331236",
    "LAFM5678A9331237",
    "LAFM5678A9331238",
];
var _SpotWeldIDs = [
    "WS1011-SPCH31291",
    "WS1011-SPCH31292",
    "WS1013-SPCH31293",
    "WS1013-SPCH31294",
    "WS1041-SPCH31295",
    "WS1042-SPCH31296",
    "WS1042-SPCH31297",
    "WS1042-SPCH31298",
    "WS1081-SPCH31299",
    "WS1081-SPCH31300",
    "WS3011-SPCH31301",
    "WS3011-SPCH31302",
    "WS5001-SPCH31303",
    "WS5001-SPCH31304",
    "WS3301-SPCH31305",
    "WS2431-SPCH31306",
    "WS3341-SPCH31307",
    "WS9012-SPCH31308",
    "WS1102-SPCH31309",
    "WS1209-SPCH31310",
    "WS8743-SPCH31312",
    "WS8734-SPCH31313",
    "WS8876-SPCH31314",
    "WS9876-SPCH31315",
    "WS1022-SPCH31316",
    "WS1023-SPCH31317",
    "WS1024-SPCH31318",
    "WS1024-SPCH31319",
    "WS1099-SPCH31320"
];
_CarBodyPics= {
    "WS1011-SPCH31291": "url(/pics/car-body0.jpg)",
    "WS1011-SPCH31292": "url(/pics/car-body1.jpg)",
    "WS1013-SPCH31293": "url(/pics/car-body2.jpg)",
    "WS1013-SPCH31294": "url(/pics/car-body3.jpg)",
    "WS1041-SPCH31295": "url(/pics/car-body4.jpg)",
    "WS1042-SPCH31296": "url(/pics/car-body5.jpg)",
    "WS1042-SPCH31297": "url(/pics/car-body6.jpg)",
    "WS1042-SPCH31298": "url(/pics/car-body7.jpg)",
    "WS1081-SPCH31299": "url(/pics/car-body8.jpg)",
    "WS1081-SPCH31300": "url(/pics/car-body9.jpg)",
    "WS3011-SPCH31301": "url(/pics/car-body10.jpg)",
    "WS3011-SPCH31302": "url(/pics/car-body11.jpg)",
    "WS5001-SPCH31303": "url(/pics/car-body12.jpg)",
    "WS5001-SPCH31304": "url(/pics/car-body13.jpg)",
    "WS3301-SPCH31305": "url(/pics/car-body14.jpg)",
    "WS2431-SPCH31306": "url(/pics/car-body15.jpg)",
    "WS3341-SPCH31307": "url(/pics/car-body16.jpg)",
    "WS9012-SPCH31308": "url(/pics/car-body17.jpg)",
    "WS1102-SPCH31309": "url(/pics/car-body18.jpg)",
    "WS1209-SPCH31310": "url(/pics/car-body19.jpg)",
    "WS8743-SPCH31312": "url(/pics/car-body20.jpg)",
    "WS8734-SPCH31313": "url(/pics/car-body21.jpg)",
    "WS8876-SPCH31314": "url(/pics/car-body22.jpg)",
    "WS9876-SPCH31315": "url(/pics/car-body23.jpg)",
    "WS1022-SPCH31316": "url(/pics/car-body2.jpg)",
    "WS1023-SPCH31317": "url(/pics/car-body3.jpg)",
    "WS1024-SPCH31318": "url(/pics/car-body4.jpg)",
    "WS1024-SPCH31319": "url(/pics/car-body5.jpg)",
    "WS1099-SPCH31320": "url(/pics/car-body6.jpg)"
}
_WeldMethods = {
    "焊接程序": ["Script 101","Program1-B","Program1-C"],
    "板材材质": ["高强钢","Program2-A","Program2-C"],
    "板材厚度": ["2.5 mm","Program3-B","Program3-C"],
    "控制方式": ["恒流","Program4-B","Program4-A"],
    "板材层数": ["二 层","Program4-B","Program4-A"],
};


_WeldParams = [
    ["预压时间",  "200 ms", "200 ms"],
    ["预热电流",  "2 KA", "2.01 KA"],
    ["预热时间",  "200 ms", "200 ms"],
    ["焊接电流",  "5 KA", "5.02 KA"],
    ["焊接时间",  "400 ms", "400 ms"],
    ["设定回火时间", "200 ms", "200 ms"],
    ["设定回火电流", "2 ka", "2 ka"],
    ["焊接压力", "350 daN", "351 daN"],
    ["二极管温度", "<80°C", "40°C"],
    ["变压器温度", "<150°C", "8.5°C"],
    ["总进水温度", "<30°C", "25°C"],
    ["总回水温度", "<30°C", "25°C"],
    ["次级焊接电流","0 - 20 Ka","7 Ka"],
    ["次级输出电压","0 - 20 V", "4 V"],
    ["总冷却循环进水流量", "0 - 20 升/分","1 升/分"],
    ["总冷却循环回水流量", "0 - 20 升/分","1 升/分"],
    ["总冷水进水压力 P1", "0-0.5 MPa",  "0.3 MPa"],
    ["总冷水回水压力 P1", "0-0.5 MPa",  "0.25 MPa"],
    ["冷却时间",    "500 ms","510 ms"],
    ["维持时间",    "500 ms","470 ms"],
    ["休止时间",    "1 ms", "1 ms"],
]



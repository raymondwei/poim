/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_TreePane = (function(){
    function AC_TreePane (rp){
        this.RP = rp;
        this.AJ_div = $('<div id="left-tree"></div>');
        this.AJ_svg = $('<svg xmlns="http://www.w3.org/2000/svg"></svg>')
            .attr({id: "the-svg", version:"1.1"})
            .appendTo(this.AJ_div);
        this.AJ_cover = $('<div id="tree-pane-cover"></div>');
        
        this.AV_tabindex = 0;
        this.AV_trees = [
            new AC_Tree(this, "prod"),
            new AC_Tree(this, "equip"),
            new AC_Tree(this, "org")
        ];
    } // end of AC_TreePane constructor

    AC_TreePane.prototype.AF_path_nodes = function(tree){
        var path = [];
        var node = this.RP.AV_focus_node;
        while(node){
            path.unshift(node);
            node = node.AV_parent;
        }
        return path;
    };// end of AC_TreePane.prototype.AF_path_nodes

    AC_TreePane.prototype.AF_show_cover = function(onoff){
        if(onoff){
            // covers the left-tree with cover.
            // set click event on the cover: bringback to rootpage-view and,
            // removec cover thereafter
            var self = this;
            this.AJ_cover.appendTo(this.RP.AJ_left).unbind("click")
            .bind("click", function(e){
                self.AF_show_cover(false);
                self.RP.tb.AF_show_rootpage_view();
                return U.AF_stop_event(e);
            });
            var path = self.AF_path_nodes(this.AV_trees[0]);
            self.AF_show_path(path);
        } else {
            this.AJ_cover.empty();
            this.AJ_cover.detach();
        }
        return this;
    };// end of AC_TreePane.prototype.AF_show_cover

    AC_TreePane.prototype.AF_show_path = function(path){
        var y = U.top_y, div, bg,
            left = '30px';
        for(var i=0; i<path.length; i++){
            bg = U.nodebg["level_" + path[i].AV_level];
            div = $('<div class="tree-node unfocus-node"></div>');
            div.css({top: y, left: '30px', "background-color": bg})
               .text(path[i].AV_synop_info.info)
               .appendTo(this.AJ_cover);
            y = y + 80;
        }
    };// end of AC_TreePane.prototype.AF_show_path

    AC_TreePane.prototype.AF_show = function(onoff){
        //this.AJ_div.empty();
        if(onoff){
            this.AJ_div.appendTo(this.RP.AJ_left);
            this.AV_trees[this.AV_tabindex].AF_show();
        } else {
            this.AJ_div.detach();
        }
        return this;
    }; // end of AC_TreePane.prototype.AF_add_node

    return AC_TreePane;
})();

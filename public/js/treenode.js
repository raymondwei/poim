/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_TreeNode = (function(){
    function AC_TreeNode(tree){
        this.RP = tree.tree_pane.RP;
        this.tree = tree;
        this.AV_parent = null;
        this.AV_level = 0;
        this.AV_top_y = U.top_y;
        this.AV_children = [];
        this.AV_lines = [];
        // jQuery DOM-object for this node
        this.AJ_div = $('<div class="tree-node unfocus-node"></div>');

        // if children are expanded. null when no child exists.
        this.AV_expand = null; 
    } // end of AC_TreeNode constructor

    AC_TreeNode.prototype.AF_setup_ehandler = function(){
        var self = this;
        this.AJ_div.unbind("click").bind("click", function(e){
            if(!self.AF_same(self.RP.AV_focus_node)){
                self.AF_focus(true);
            } else {
                if( self.AV_children.length > 0){
                    for(var i=0; i<self.AV_children.length; i++){
                        self.AV_children[i].AF_show(true);
                    }
                }
            }           
            return U.AF_stop_event(e);        
        });
    };// end of AC_TreeNode.prototype.AF_setup_ehandler

    AC_TreeNode.prototype.AF_populate = function(dic){
        this.AJ_div.css("cursor", "pointer");
        this.AV_synop_info = { picurl: dic.picurl, info: dic.name };
        return this;
    };// end of AC_TreeNode.prototype.AF_populate
    
    AC_TreeNode.prototype.AF_set_lines = function(srank){//siblings-rank:0..N-1
        var lh, lv;
        if(this.AV_level > 0){
            if(srank === 0){
                lh = $('<div class="hlinediv1"></div>');
                lh.css({top:  (this.AV_top_y + 28) + 'px',
                        left: (this.AV_level * 200 - 23) + 'px'});
                this.AV_lines.push(lh);
            } else {
                lh = $('<div class="hlinediv2"></div>');
                lh.css({top:  (this.AV_top_y + 28) + 'px',
                        left:  this.AV_level * 200 + 'px'});
                this.AV_lines.push(lh);
                lv = $('<div class="vlinediv"></div>');
                lv.css({top:  (this.AV_top_y - 36) + 'px',
                        left: this.AV_level * 200 + 'px' });
                this.AV_lines.push(lv);
            }
        }
        /**
        var _x1, _y1, _x2, _y2, line1, line2;
        if(this.AV_level > 0){
            if(srank === 0){
                _x1 = this.AV_level * U.col_width - 30;
                _x2 = _x1 + 60;
                _y1 = this.AV_top_y + 13;
                _y2 = _y1;
                line1 = $('<line class="white-line"></line>');
                line1.attr({x1: _x1+"", x2:_x2+"", y1: _y1+"", y2: _y2+"" });
                //     .appendTo($('#the-svg'));
                this.AV_lines.push(line1);
            } else {
                _x1 = this.AV_level * U.col_width;
                _x2 = _x1 + 30;
                _y1 = this.AV_top_y + 13;
                _y2 = _y1;
                line1 = $('<line class="white-line"></line>');
                line1.attr({x1: _x1+"", x2:_x2+"", y1: _y1+"", y2: _y2+"" });
                //     .appendTo($('#the-svg'));
                this.AV_lines.push(line1);
            }
        }*/
        return this;
    };// end of AC_TreeNode.prototype.AF_set_lines

    AC_TreeNode.prototype.AF_addchild = function(dic){
        var node = new AC_TreeNode(this.tree);
        node.AF_populate(dic);
        node.AV_level = this.AV_level + 1;
        node.AV_parent = this;
        node.AV_top_y = this.AV_top_y + 
                        this.AV_children.length * (U.node_height + U.ydist);
        node.AF_set_lines(this.AV_children.length);
        this.AV_children.push(node);
        return node;
    };// end of AC_TreeNode.prototype.AF_addchild

    AC_TreeNode.prototype.AF_show = function(onoff){
        if(!onoff){
            this.AJ_div.detach();
            for(var i=0; i<this.AV_lines.length; i++){
                this.AV_lines[i].detach();
            }
            if(this.AV_children.length > 0){
                for(var i=0; i<this.AV_children.length; i++){
                    this.AV_children[i].AF_show(false);
                }
            }
        } else {
            var bg = U.nodebg["level_" + this.AV_level];
            var _left = this.AV_level * 200 + 30;
            var _top = U.top_y;
            var self = this;
            if(this.AV_level > 0){
                var siblings = this.AV_parent.AV_children;
                for(var i=0; i<siblings.length; i++){
                    if(this === siblings[i]){
                        _top = siblings[i].AV_top_y;
                    }
                }
            }
            this.AJ_div.css({
                top: _top,
                left: _left + "px",
                "background-color": bg
            })
            .text(self.AV_synop_info.info)
            .appendTo(this.tree.AJ_div); // attach to tree_pane > div#left-tree
            this.AF_setup_ehandler();

            for(var i=0; i<this.AV_lines.length; i++){
                var line = this.AV_lines[i];
                line.appendTo(this.tree.AJ_div);
                // $('#the-svg').append(this.AV_lines[i]);
            }

            return this;
        }
    };//end of AC_TreeNode.prototype.AF_show

    AC_TreeNode.prototype.AF_same = function(node){
        if(!node) return false;
        var res = this.AV_synop_info.picurl === node.AV_synop_info.picurl &&
                  this.AV_synop_info.info   === node.AV_synop_info.info;
        return res;
    };// end of AC_TreeNode.prototype.AF_same

    AC_TreeNode.prototype.AF_focus = function(v){
        if(typeof(v) === "undefined"){
            return this.AF_same(this.RP.AV_focus_node);
        } else {
            // v is given. If prev-focus existed, set that to be not-focus
            if( this.RP.AV_focus_node &&
                !this.AF_same(this.RP.AV_focus_node)){
                this.RP.AV_focus_node.AF_focus(false);
                var siblings = this.AV_parent.AV_children;
                for(var i=0; i<siblings.length; i++){
                    var sib = siblings[i];
                    if( sib !== this && sib.AV_expand){
                        for(var j=0; j<sib.AV_children.length; j++){
                            sib.AV_children[j].AF_show(false);
                        }                        
                    }
                }
            }
            // set visual for focus: flase or true
            if(v){
                this.RP.AV_focus_node = this;
                this.RP.tb.synop.AF_show(this.AV_synop_info);
                this.AJ_div.removeClass("unfocus-node").addClass("focus-node");
                if( this.AV_children.length > 0){
                    this.AV_expand = true;
                    for(var i=0; i<this.AV_children.length; i++){
                        this.AV_children[i].AF_show(true);
                    }
                }
            } else {
                this.AJ_div.removeClass("focus-node").addClass("unfocus-node");
            }
        }
    };// end of AC_TreeNode.prototype.AF_focus

    AC_TreeNode.prototype.AF_expand = function(v){

    };// end of position

    return AC_TreeNode;
})();
/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_Synop = (function(){
    function AC_Synop(rp){
        this.RP = rp;
        this.AJ_div = $('#titlebar-synop');
        this.AJ_picframe = $('#synop-minipic');
        this.AJ_infobox = $('#synop-infobox');
        this.AJ_picframe = $('<img id="synop-pic">')
            .appendTo(this.AJ_picframe);
        this.AJ_info = $('<div id="synop-info"></div>')
            .appendTo(this.AJ_infobox);
    }

    AC_Synop.prototype.AF_show = function(dic){
        if(typeof(dic) === "undefined"){
            this.AF_set_visible(false);
        } else {
            this.AF_set_visible(true);
            $('#synop-pic').attr("src", dic.picurl);
            $('#synop-info').text(dic.info);
        }
        return this;
    }; // end of AC_Synop.prototype.AF_add_node

    // when in detailed view, synop will be hidden
    // when in rootpage-view, synop will be visible
    AC_Synop.prototype.AF_set_visible = function(t){
        if(t){
            this.AJ_div.css('visibility', 'visible');
            if( this.RP.AV_focus_node && 
                this.RP.AV_focus_node.AV_level > 0){
                    this.RP.tb.AJ_i40logo.css('right', '370px');
            }
        } else {
            this.AJ_div.css('visibility','hidden');
            this.RP.tb.AJ_i40logo.css('right', '20px');
        }
        return this;
    };// end of AC_Synop.prototype.AF_set_visible

    return AC_Synop;
})();
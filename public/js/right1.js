/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_Right1 = (function(){
    function AC_Right1(rp, right_pane){
        this.RP = rp;
        this.right_pane = right_pane;
        this.AJ_div = $('<div id="right1"></div>');
        this.AJ_cover = $('<div id="right1a"></div>');
        this.AJ_caldiv = $('<div id="calendar-pane"></div>');
        this.AV_right_width = this.RP.body_w - 200; // tree-cover(200)
        var calwid = this.AV_right_width - 280;// right-date-col(200)
        var right1_lower_height = this.RP.body_h - 426 - 70;
        this.AJ_vintbar = $('<div id="vin-tbar"></div>').appendTo(this.AJ_div)
            .text("车体序号");

        this.AJ_caldiv.css({
            width: calwid, 
            height: '350px'})
            .appendTo(this.AJ_div);
        this.AJ_carpic = $('<div id="right1-picframe"></div>');
        var pic_h = this.RP.body_h - 426 - 70;
        this.AJ_carpic.css({width: calwid, height: pic_h})
            .appendTo(this.AJ_div);

        // date picked
        this.AJ_date = $('<div id="right-date"></div>')
            .text("生产日期: 2019-09-11")
            .appendTo(this.AJ_div);
        this.AJ_vin = $('<div id="vin-titlebar"></div>').appendTo(this.AJ_div);
        this.AJ_vins = $('<div id="right-vins"></div>').appendTo(this.AJ_div);
        this.AJ_procs = $('<div id="proc-pane"></div>').appendTo(this.AJ_div);

        this.AJ_ws_tbar = $('<div id="ws-tbar"></div>').appendTo(this.AJ_div);
        this.AJ_wstations = $('<div id="wstation-pane"></div>').appendTo(this.AJ_div);
        
        this.AJ_wstations.css({height: (this.RP.body_h - 450) + 'px'});
        for(var i=0; i<_VINs.length; i++){
            $('<div class="vin-box"></div>')
            .attr("id",_VINs[i])
            .text(_VINs[i])
            .appendTo(this.AJ_vins);
        }
        this.AJ_cover = $('<div id="right1a"></div>');
        this.AV_cover = false;
    }// end of AC_Right1 constructor

    AC_Right1.prototype.AF_setup_procs = function(){
        this.AJ_procs.empty();
        var div1;
        div1 = $('<div id="metal" class="proc-box"></div>');
        $('<div class="proc-pic"></div>')
        .css({"background-image": "url(/pics/metalwork.jpg)"})
        .appendTo(div1);
        $('<div class="proc-name"></div>').text("冲压")
        .appendTo(div1);
        div1.appendTo(this.AJ_procs);
 
        var div2;
        div2 = $('<div id="weld" class="proc-box"></div>');
        $('<div class="proc-pic"></div>')
        .css({"background-image": "url(/pics/zanger.png)"})
        .appendTo(div2);
        $('<div class="proc-name"></div>').text("焊接")
        .appendTo(div2);
        div2.appendTo(this.AJ_procs);

        var div3;
        div3 = $('<div id="paint" class="proc-box"></div>');
        $('<div class="proc-pic"></div>')
        .css({"background-image": "url(/pics/paint.jpg)"})
        .appendTo(div3);
        $('<div class="proc-name"></div>').text("涂装")
        .appendTo(div3);
        div3.appendTo(this.AJ_procs);

        var div4;
        div4 = $('<div id="assembly" class="proc-box"></div>');
        $('<div class="proc-pic"></div>')
        .css({"background-image": "url(/pics/assembly.jpg)"})
        .appendTo(div4);
        $('<div class="proc-name"></div>').text("总装")
        .appendTo(div4);
        div4.appendTo(this.AJ_procs);

        var box, self = this;
        // div2 (spot welding): when clicked, show weld-spots
        div2.unbind("click").bind("click", function(e){
            self.AJ_ws_tbar.text("焊接工作站 - 焊点序号");
            self.AJ_wstations.empty();
            for(var i=0; i<_SpotWeldIDs.length; i++){
                box = $('<div class="wstation-box"></div>').appendTo(self.AJ_wstations);
                box.text(_SpotWeldIDs[i]);
                box.attr('id', _SpotWeldIDs[i]);
                box.unbind("click").bind("click", function(ee){
                    var picname = _CarBodyPics[($(this).attr('id'))];
                    $('#right1-picframe').css("background-image", picname);
                });

                box.unbind("dblclick").bind("dblclick", function(ee){
                    self.right_pane.AV_ws_id = $(this).attr("id");
                    self.right_pane.AF_show(2);
                    return U.AF_stop_event(ee);
                })
            }
            return U.AF_stop_event(e);
        });
    };// end of AC_Right1.prototype.AF_setup_procs

    AC_Right1.prototype.AF_setup_page = function(){
        this.AJ_div.css({width: this.AV_right_width})
            .appendTo(this.RP.AJ_right);
        var self = this;
        $('div.vin-box').unbind('click').bind('click', function(e){
            self.right_pane.AJ_vin = $(this).attr("id");
            self.AJ_vin.text($(this).attr("id"));
            self.AF_setup_procs();
        });
    };// end of AC_Right1.prototype.AF_setup_page

    AC_Right1.prototype.AF_show_cover = function(){
        var node;
        this.AJ_cover.empty().appendTo(this.RP.AJ_right);
        
        node = $('<div class="cover-node">2019-09-11</div>')
                .css({top: "30px", left: "10px"})
                .appendTo(this.AJ_cover);
        node = $('<div class="cover-node">LAFM5678A9331241</div>')
                .css({top: "110px", left: "10px",     
                      "background-color": "rgb(75, 75, 75)" })
                .appendTo(this.AJ_cover);
        node = $('<div class="cover-node">焊点:WS1011-SPCH31291</div>')
                .css({top: "190px", left: "10px", "background-color": "rgb(60, 60, 60)"})
                .appendTo(this.AJ_cover);
        this.AV_cover = true;
        var self = this;
        this.AJ_cover.unbind('click').bind('click', function(e){
            self.AJ_div.appendTo(self.RP.AJ_right);
            self.AF_show();
            return U.AF_stop_event(e);
        });
    };// end of AC_Right1.prototype.AF_show_cover

    AC_Right1.prototype.AF_show = function(){
        var self = this;
        if(!this.AV_cover){  // r2 not shown. This is inital rendering
            this.AF_setup_page();
        } else { // 
            this.AJ_cover.detach();
            this.AJ_div.appendTo(this.RP.AJ_right);
            this.AJ_div.animate({width: this.AV_right_width + 'px'}, function(e){
                self.AV_cover = false;
                return U.AF_stop_event(e);
            })
        }
    };// end of AC_Right1.prototype.AF_show

    AC_Right1.prototype.AF_shrink = function(){
        var self = this;
        this.AJ_div.animate({width: '0px'}, function(e){
            self.AJ_div.detach();
            self.AF_show_cover();
            return U.AF_stop_event(e);
        });
    };// end of AC_Right1.prototype.AF_shrink

    return AC_Right1;
})();
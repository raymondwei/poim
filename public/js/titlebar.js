/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_Titlebar = (function() {
    // possible target_id: 'FTSUPER' or <pid> of master-pa
    function AC_Titlebar(RP) {
        this.RP = RP;
        this.AJ_titlebar = $('#poim-titlebar');
        this.AJ_i40logo = $('<img id="i40-logo"/>')
             .attr('src','/pics/i40-logo.png')
             .appendTo(this.AJ_titlebar);
        this.AJ_userhead = $('<img id="user-head"/>');
        this.AJ_userhead
            .attr("src","/pics/male-head.png")
            .css("display","none")
            .appendTo(this.AJ_titlebar);
        this.AJ_username = $('<div id="user-name"></div>');
        this.AJ_username
            .text("武策远").css("display", "none")
            .appendTo(this.AJ_titlebar);
        RP.tb = this;
        this.synop = new AC_Synop(this.RP);

        this.AF_setready_detailview();
        this.AJ_loginout = $('#loginout');

        this.synop.AF_set_visible(false);

        var self = this;
        this.AJ_loginout.unbind('click').bind('click',function(e){
            self.RP.AV_loggedin = !self.RP.AV_loggedin;
            self.RP.AF_set();
            if(self.RP.AV_loggedin){
                $('#loginout').text("退出登录");
                self.AJ_userhead.css("display","block");
                self.AJ_username.css("display","block");
            } else {
                self.AJ_userhead.css("display","none");
                self.AJ_username.css("display","none");
                self.synop.AF_set_visible(false);
                $('#loginout').text("登录");
            }
            return U.AF_stop_event(e);
        })
    }; // end of AC_Titlebar constructor

    AC_Titlebar.prototype.AF_setready_detailview = function(){
        var self = this; 
        $('#titlebar-synop').unbind('click').bind('click', function(e){
            if(self.RP.AV_focus_node.AV_level === 3){
                self.AF_show_detailview();
            }
            return U.AF_stop_event(e);
        });        
    };// end of AC_Titlebar.prototype.AF_setready_detailview

    AC_Titlebar.prototype.AF_setready_rootpage_view = function(){
        /*
        var self = this;
        $('#poim-titlebar').unbind('click').bind('click', function(e){
            self.AF_show_rootpage_view();
            return U.AF_stop_event(e);
        })
        */
        this.RP.tree_pane.AF_show_cover(true);  
    };// end of AC_Titlebar.prototype.AF_setready_rootpage_view

    AC_Titlebar.prototype.AF_show_detailview = function(){
        var self = this;
        $('#titlebar-synop').css({display: 'none'});
        //$('#company-name').css('display', 'none');
        $('#company-name')
         .text(self.RP.AV_focus_node.AV_synop_info.info + self.RP.AV_title_text);
        $('#i40-logo').css('right','10px');
        var w = self.RP.body_w - U.col_width;

        // render right_pane
        this.RP.right_pane.AF_show(1);

        $('#container-left').animate(
            {width: U.col_width + 'px'}, U.animation_duration, function(e){
                self.AF_setready_rootpage_view();
                return U.AF_stop_event(e);
            });
        $('#container-right').animate(
            {width: w + 'px'}, U.animation_duration, function(e){}
        );
    };// end of AC_Titlebar.prototype.AF_show_detailview
  
    AC_Titlebar.prototype.AF_show_rootpage_view = function(){
        var self = this; 
        $('#poim-titlebar').unbind('click');
        $('#container-left').animate(
            {width: '100%'}, U.animation_duration, function(e){
                self.AF_setready_detailview();
                $('#titlebar-synop').css({display: 'block'});
                $('#company-name').css('display', 'block').text(_DB.root.name);
                $('#i40-logo').css('right','370px');
                return U.AF_stop_event(e);
            }
        );
        $('#container-right').animate(
            {width: '0%'}, U.animation_duration, function(e){} );
        $('#titlebar-synop').unbind('click').bind('click', function(e){
            self.AF_detailview();
        });
    };// end of AC_Titlebar.prototype.AF_show_rootpage_view

    AC_Titlebar.prototype.cid = function() {  return "TB";  };
    
    // this will be called async after init loads all initial ents
    // and this.idbs are opened
    AC_Titlebar.prototype.AF_init = function() {
        this.AF_resize();
    }; // end of AC_Titlebar.AF_init method
    
    AC_Titlebar.prototype.AF_resize = function() {
    }; // end of AC_Titlebar.prototype.AF_resize
  
    return AC_Titlebar;
  })();
  
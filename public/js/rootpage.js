/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_RootPage = (function() {
    // possible target_id: 'FTSUPER' or <pid> of master-pa
    function AC_RootPage() {
        this.AV_loggedin = false;
        this.body_w = parseInt($('body').css('width'));
        this.body_h = parseInt($('body').css('height'));
        this.tb = new AC_Titlebar(this);

        this.AJ_div = $('#poim-container');
        this.AJ_left = $('#container-left');
        this.AJ_right = $('#container-right');
        this.AV_focus_node = null;

        this.tree_pane = new AC_TreePane(this);
        this.public_pane = new AC_PublicPane(this);
        this.right_pane = new AC_RightPane(this);

        this.AF_set();
        this.AV_title_text = " - 制造过程监控与追溯"
    }; // end of AC_RootPage constructor
    
    AC_RootPage.prototype.AF_set = function(){
      this.AJ_left.empty();
      if(this.AV_loggedin){
        this.public_pane.AF_show(false);
        this.tree_pane.AF_show(true);
      } else {
        this.tree_pane.AF_show(false);
        this.public_pane.AF_show(true);
      }
    };// end of AC_RootPage.prototype.AF_set

    AC_RootPage.prototype.AF_init = function() {
      this.AF_resize();
    }; // end of AC_RootPage.AF_init method
  
    AC_RootPage.prototype.AF_resize = function() {
      this.body_w = parseInt($('body').css('width'));
      this.body_h = parseInt($('body').css('height'));
    }; // end of AC_RootPage.prototype.AF_resize
  
    return AC_RootPage;
  })();
  
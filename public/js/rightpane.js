/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var AC_RightPane = (function(){
    function AC_RightPane(rp){
        this.RP = rp; // rp.AJ_right is the parent div
        this.right1 = new AC_Right1(rp, this);
        this.right2 = new AC_Right2(rp, this);
        this.AV_date = "2019-09-11";
        this.AV_vin = "";
        this.AV_ws_id = "";
    }// end of AC_RightPane constructor

    AC_RightPane.prototype.AF_show = function(v){
        //this.RP.AJ_right.empty();
        if(typeof(v) === "undefined") 
            return;
        if(v === 1){
            // when right2.AV_shown, right1: remove right1a, and 
            // let right12 animated width -> 0
            // if right2 not shown, render right1 without animation
            this.right1.AF_show(this.right2.AV_shown);
        } else if(v === 2){
            this.right2.AF_show(true);
            // when right1 shrunk to 200, put on right1a
            this.right1.AF_shrink(); 
        }
    };// end of AC_RightPane.prototype.AF_show
    
    return AC_RightPane;
})();
﻿/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"

var U = (function() {
  function U() {
  }

  U.nodebg = {
      level_0: "rgb(70, 70, 90)",
      level_1: "rgb(60, 60, 90)",
      level_2: "rgb(50, 50, 90)",
      level_3: "rgb(40, 40, 90)"
  }

  U.col_width = 200;

  U.top_y = 30;
  U.ydist = 40;
  U.node_height = 26;

  // used in event handler: stop e from bubbled up
  U.animation_duration = 2000;

  U.AF_stop_event = function(e) {
    if (e) {
      e.bubbles = false;
      if (e.stopPropagation) e.stopPropagation();
      e.preventDefault();
    }
    return false;
  }; // end of static method U.AF_stop_event
  
  return U;
})();
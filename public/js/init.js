/***********************************************
 *
 * COPYRIGHT I40 2019
 * ALL RIGHTS RESERVED
 *
 ************************************************
 * Author: Raymond Wei  2019
 * --------------------------------------------- */
"use strict"
 var RP;

$(document).ready(function() {
  
    $(window).on("resize", function() {
      if(RP) 
        RP.AF_resize();
    });
  
    var jpug = $('#pugdata');
    var data_str = jpug.text();
    RP = new AC_RootPage();
    jpug.empty();
}); // end of $(document).ready(function() {
  